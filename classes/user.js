class User {
  _name;

  _socketKey;

  _subscribedToUsersList;

  _ws;

  constructor(socketKey, connection) {
    this._name = socketKey;
    this._socketKey = socketKey;
    this._ws = connection;
    this._subscribedToUsersList = false;
  }

  get name() {
    return this._name;
  }

  setUserName(name) {
    this._name = name;
  }

  get socketKey() {
    return this._socketKey;
  }

  get ws() {
    return this._ws;
  }

  get subscribedToUsersList() {
    return this._subscribedToUsersList;
  }

  setSubscribeToOnlineList(isSubscribed) {
    this._subscribedToUsersList = isSubscribed;
    return true;
  }
}

module.exports = User;
