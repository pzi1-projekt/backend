class Chat {
  _user1;

  _user2;

  constructor(user1, user2) {
    this._user1 = user1;
    this._user2 = user2;
  }

  get user1() {
    return this._user1;
  }

  get user2() {
    return this._user2;
  }
}

module.exports = Chat;
