const { ceChatAcceptRequest, ceChatClose, ceChatDeleteRequest, ceChatRequest, ceChatSendMessage } = require('./eventHandlers/chat');
const { ceUserSetName, ceUserSubscribeList, ceUserUnsubscribeList } = require('./eventHandlers/user');
const { CE_KEYS } = require('./keys');
const { GENERAL_MESSAGES } = require('./messages');

const websocketEventHandler = (
  user, eventKey, eventGroup, eventData
) => {
  try {
    switch (eventGroup) {
      case 'user':
        switch (eventKey) {
          case CE_KEYS.USER.SET_NAME:
            ceUserSetName(user, eventData);
            break;

          case CE_KEYS.USER.SUBSCRIBE_LIST:
            ceUserSubscribeList(user);
            break;

          case CE_KEYS.USER.UNSUBSCRIBE_LIST:
            ceUserUnsubscribeList(user);
            break;

          default:
            throw new Error('Command not found');
        }
        break;

      case 'chat':
        switch (eventKey) {
          case CE_KEYS.CHAT.ACCEPT_REQUEST:
            ceChatAcceptRequest(user, eventData);
            break;

          case CE_KEYS.CHAT.DELETE_REQUEST:
            ceChatDeleteRequest(user, eventData);
            break;

          case CE_KEYS.CHAT.REQUEST:
            ceChatRequest(user, eventData);
            break;

          case CE_KEYS.CHAT.SEND_MESSAGE:
            ceChatSendMessage(user, eventData);
            break;

          case CE_KEYS.CHAT.CLOSE:
            ceChatClose(user);
            break;

          default:
            throw new Error('Command not found');
        }
        break;

      default: throw new Error('Command not found');
    }
  } catch (e) {
    console.log(e);
    user.ws.send(GENERAL_MESSAGES.UNKNOWN_MESSAGE);
  }
};

module.exports = websocketEventHandler;
