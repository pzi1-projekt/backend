const { CHAT_MESSAGES } = require('../../messages');
const ChatClosure = require('../../../closures/chat');
const UsersClosure = require('../../../closures/users');

const ceChatRequest = (user, data) => {
  const { socketKey } = data;
  if (socketKey && typeof socketKey === 'string') {
    const userToRequestChatWith = UsersClosure.getUserByKey(socketKey);
    if (userToRequestChatWith && userToRequestChatWith.socketKey) {
      const { foundChat } = ChatClosure.getUserChat(userToRequestChatWith);
      if (foundChat) {
        return user.ws.send(CHAT_MESSAGES.SE.REQUEST.USER_ALREADY_IN_CHAT);
      }
      if (!ChatClosure.createChatRequest(user, userToRequestChatWith)) {
        return user.ws.send(CHAT_MESSAGES.SE.REQUEST.USER_ALREADY_IN_CHAT);
      }
      return userToRequestChatWith.ws.send(CHAT_MESSAGES.IE.CHAT_REQUEST(user.socketKey, user.name));
    }
    return user.ws.send(CHAT_MESSAGES.SE.REQUEST.INCORRECT_KEY);
  }
  return user.ws.send(CHAT_MESSAGES.SE.REQUEST.INCORRECT_KEY);
};

const ceChatAcceptRequest = (user, data) => {
  const { socketKey } = data;
  if (socketKey && typeof socketKey === 'string') {
    try {
      const chatRequestAccepted = ChatClosure.acceptChatRequest(socketKey);
      if (chatRequestAccepted) {
        const user2 = UsersClosure.getUserByKey(socketKey);
        ChatClosure.createChat(user, user2);
        user2.ws.send(CHAT_MESSAGES.SE.REQUEST.ACCEPTED(user.socketKey, user.name));
        return user.ws.send(CHAT_MESSAGES.SE.ACCEPT_REQUEST.SUCCESS(user2.socketKey, user2.name));
      }
      return user.ws.send(CHAT_MESSAGES.SE.ACCEPT_REQUEST.NOT_FOUND);
    } catch (e) {
      return user.ws.send(CHAT_MESSAGES.SE.ACCEPT_REQUEST.ERROR);
    }
  }
  return user.ws.send(CHAT_MESSAGES.SE.ACCEPT_REQUEST.NOT_FOUND);
};

const ceChatDeleteRequest = (user, data) => {
  const { socketKey } = data;
  if (socketKey && typeof socketKey === 'string') {
    try {
      const chatRequestDeleted = ChatClosure.declineChatRequest(socketKey);
      if (chatRequestDeleted) {
        const user2 = UsersClosure.getUserByKey(socketKey);
        user2.ws.send(CHAT_MESSAGES.SE.REQUEST.DECLINED(user.socketKey, user.name));
        return user.ws.send(CHAT_MESSAGES.SE.DELETE_REQUEST.SUCCESS(user2.socketKey, user2.name));
      }
      console.log('MRALE');
      return user.ws.send(CHAT_MESSAGES.SE.DELETE_REQUEST.NOT_FOUND);
    } catch (e) {
      return user.ws.send(CHAT_MESSAGES.SE.DELETE_REQUEST.ERROR);
    }
  }
  return user.ws.send(CHAT_MESSAGES.SE.DELETE_REQUEST.NOT_FOUND);
};

const ceChatSendMessage = (user, data) => {
  const { message } = data;
  if (message && typeof message === 'string' && message.length <= 2000) {
    try {
      // Check if user in chat
      const chatRecipient = ChatClosure.getChatRecipient(user);
      if (chatRecipient) {
        chatRecipient.ws.send(CHAT_MESSAGES.IE.MESSAGE_RECEIVED(user.socketKey, message));
        return user.ws.send(CHAT_MESSAGES.SE.SEND_MESSAGE.SUCCESS(message));
      }
      return user.ws.send(CHAT_MESSAGES.SE.SEND_MESSAGE.NOT_IN_CHAT);
    } catch (e) {
      return user.ws.send(CHAT_MESSAGES.SE.SEND_MESSAGE.ERROR);
    }
  }
  return user.ws.send(CHAT_MESSAGES.SE.SEND_MESSAGE.NOT_MESSAGE);
};

const ceChatClose = (user) => {
  try {
    const { foundChat, isUser1 } = ChatClosure.getUserChat(user);
    if (foundChat) {
      if (isUser1) {
        foundChat.user1.ws.send(CHAT_MESSAGES.SE.CLOSE.SUCCESS);
        foundChat.user2.ws.send(CHAT_MESSAGES.IE.CLOSED(foundChat.user1.socketKey, foundChat.user1.name));

        ChatClosure.deleteChat(user);

        return;
      }

      foundChat.user2.ws.send(CHAT_MESSAGES.SE.CLOSE.SUCCESS);
      foundChat.user1.ws.send(CHAT_MESSAGES.IE.CLOSED(foundChat.user2.socketKey, foundChat.user2.name));

      ChatClosure.deleteChat(user);
      return;
    }
    return user.ws.send(CHAT_MESSAGES.SE.CLOSE.NOT_IN_CHAT);
  } catch (e) {
    return user.ws.send(CHAT_MESSAGES.SE.CLOSE.ERROR);
  }
};

module.exports = {
  ceChatRequest,
  ceChatAcceptRequest,
  ceChatSendMessage,
  ceChatClose,
  ceChatDeleteRequest
};
