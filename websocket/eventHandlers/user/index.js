const { USER_MESSAGES } = require('../../messages');
const UsersClosure = require('../../../closures/users');

const ceUserSetName = (user, data) => {
  const { name } = data;
  if (name && typeof name === 'string' && name.length <= 24) {
    try {
      user.setUserName(name);
      UsersClosure.informUsers(USER_MESSAGES.IE.NAME_UPDATED(user.socketKey, user.name), user.socketKey);
      return user.ws.send(USER_MESSAGES.SE.SET_NAME.SUCCESS(user.name));
    } catch (e) {
      return user.ws.send(USER_MESSAGES.SE.SET_NAME.ERROR);
    }
  }
  return user.ws.send(USER_MESSAGES.SE.SET_NAME.NOT_USERNAME);
};

const ceUserSubscribeList = (user) => {
  try {
    user.setSubscribeToOnlineList(true);
    const usersList = UsersClosure.getAllUsers(user.socketKey).map(userFromList => ({ name: userFromList.name, socketKey: userFromList.socketKey }));
    return user.ws.send(USER_MESSAGES.SE.SUBSCRIBE_LIST.SUCCESS(usersList));
  } catch (e) {
    return user.ws.send(USER_MESSAGES.SE.SUBSCRIBE_LIST.ERROR);
  }
};

const ceUserUnsubscribeList = (user) => {
  try {
    user.setSubscribeToOnlineList(false);
    return user.ws.send(USER_MESSAGES.SE.UNSUBSCRIBE_LIST.SUCCESS);
  } catch (e) {
    return user.ws.send(USER_MESSAGES.SE.UNSUBSCRIBE_LIST.ERROR);
  }
};

module.exports = { ceUserSetName, ceUserSubscribeList, ceUserUnsubscribeList };
