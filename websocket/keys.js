const CE_KEYS = {
  USER: {
    SET_NAME: 'ce/user/set_name',
    SUBSCRIBE_LIST: 'ce/user/subscribe_list',
    UNSUBSCRIBE_LIST: 'ce/user/unsubscribe_list'
  },
  CHAT: {
    SEND_MESSAGE: 'ce/chat/send_message',
    REQUEST: 'ce/chat/request',
    DELETE_REQUEST: 'ce/chat/delete_request',
    ACCEPT_REQUEST: 'ce/chat/accept_request',
    CLOSE: 'ce/chat/close'
  }
};

const SE_KEYS = {
  USER: {
    SET_NAME: 'se/user/set_name',
    SUBSCRIBE_LIST: 'se/user/subscribe_list',
    UNSUBSCRIBE_LIST: 'se/user/unsubscribe_list'
  },
  CHAT: {
    SEND_MESSAGE: 'se/chat/send_message',
    REQUEST: 'se/chat/request',
    DELETE_REQUEST: 'se/chat/delete_request',
    ACCEPT_REQUEST: 'se/chat/accept_request',
    CLOSE: 'se/chat/close'
  }
};

const IE_KEYS = {
  USER: {
    NAME_UPDATED: 'ie/user/name_updated',
    INFORMATION: 'ie/user/information'
  },
  CHAT: {
    MESSAGE_RECEIVED: 'ie/chat/message_received',
    CHAT_REQUEST: 'ie/chat/chat_request',
    CLOSED: 'ie/chat/closed'
  },
  GENERAL: {
    WRONG_MESSAGE_FORMAT: 'ie/general/wrong_message_format',
    UNKNOWN_MESSAGE: 'ie/general/unknown_message'
  }
};

module.exports = {
  CE_KEYS,
  SE_KEYS,
  IE_KEYS
};
