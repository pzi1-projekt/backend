const { IE_KEYS, SE_KEYS } = require('./keys');

const GENERAL_MESSAGES = {
  WRONG_MESSAGE_FORMAT: JSON.stringify({
    key: IE_KEYS.GENERAL.WRONG_MESSAGE_FORMAT,
    data: {},
    status: {
      code: 401,
      message: 'The message format is incorrect, you must use JSON!'
    }
  }),
  UNKNOWN_MESSAGE: JSON.stringify({
    key: IE_KEYS.GENERAL.UNKNOWN_MESSAGE,
    data: {},
    status: {
      code: 404,
      message: 'The sent event message is unknown'
    }
  })
};

const USER_MESSAGES = {
  SE: {
    SET_NAME: {
      SUCCESS: name => JSON.stringify({
        key: SE_KEYS.USER.SET_NAME,
        data: {
          name
        },
        status: {
          code: 200,
          message: 'Name successfuly set!'
        }
      }),
      NOT_USERNAME: JSON.stringify({
        key: SE_KEYS.USER.SET_NAME,
        data: {},
        status: {
          code: 400,
          message: 'Name must be string, max 24 characters!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.USER.SET_NAME,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    },
    SUBSCRIBE_LIST: {
      SUCCESS: users => JSON.stringify({
        key: SE_KEYS.USER.SUBSCRIBE_LIST,
        data: {
          users
        },
        status: {
          code: 200,
          message: 'Subscribed to online users list successful!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.USER.SUBSCRIBE_LIST,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    },
    UNSUBSCRIBE_LIST: {
      SUCCESS: JSON.stringify({
        key: SE_KEYS.USER.UNSUBSCRIBE_LIST,
        data: {},
        status: {
          code: 200,
          message: 'Successfuly unsubscribed from online users list!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.USER.UNSUBSCRIBE_LIST,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    }
  },
  IE: {
    NAME_UPDATED: (socketKey, name) => JSON.stringify({
      key: IE_KEYS.USER.NAME_UPDATED,
      data: {
        name,
        socketKey
      }
    }),
    INFORMATION: (socketKey, name) => JSON.stringify({
      key: IE_KEYS.USER.INFORMATION,
      data: {
        name,
        socketKey
      }
    })
  }
};

const CHAT_MESSAGES = {
  SE: {
    SEND_MESSAGE: {
      SUCCESS: message => JSON.stringify({
        key: SE_KEYS.CHAT.SEND_MESSAGE,
        data: {
          message
        },
        status: {
          code: 200,
          message: 'Message succesfuly sent!'
        }
      }),
      NOT_MESSAGE: JSON.stringify({
        key: SE_KEYS.CHAT.SEND_MESSAGE,
        data: {},
        status: {
          code: 400,
          message: 'Message must not be longer than 2000 characters!'
        }
      }),
      NOT_IN_CHAT: JSON.stringify({
        key: SE_KEYS.CHAT.SEND_MESSAGE,
        data: {},
        status: {
          code: 404,
          message: 'You are not chatting with anyone!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.CHAT.SEND_MESSAGE,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    },
    REQUEST: {
      ACCEPTED: (socketKey, userName) => JSON.stringify({
        key: SE_KEYS.CHAT.REQUEST,
        data: {
          socketKey,
          userName
        },
        status: {
          code: 200,
          message: 'Chat request accepted!'
        }
      }),
      DECLINED: (socketKey, userName) => JSON.stringify({
        key: SE_KEYS.CHAT.REQUEST,
        data: {
          socketKey,
          userName
        },
        status: {
          code: 401,
          message: 'Chat request declined!'
        }
      }),
      INCORRECT_KEY: JSON.stringify({
        key: SE_KEYS.CHAT.REQUEST,
        data: {},
        status: {
          code: 404,
          message: 'User key is incorrect!'
        }
      }),
      USER_ALREADY_IN_CHAT: JSON.stringify({
        key: SE_KEYS.CHAT.REQUEST,
        data: {},
        status: {
          code: 402,
          message: 'User already in chat!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.CHAT.REQUEST,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    },
    ACCEPT_REQUEST: {
      SUCCESS: (socketKey, userName) => JSON.stringify({
        key: SE_KEYS.CHAT.ACCEPT_REQUEST,
        data: {
          socketKey,
          userName
        },
        status: {
          code: 200,
          message: 'Chat request accepted succesfuly!'
        }
      }),
      NOT_FOUND: JSON.stringify({
        key: SE_KEYS.CHAT.ACCEPT_REQUEST,
        data: {},
        status: {
          code: 404,
          message: 'Chat request with that user is not found or user does not exist!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.CHAT.ACCEPT_REQUEST,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    },
    DELETE_REQUEST: {
      SUCCESS: (socketKey, userName) => JSON.stringify({
        key: SE_KEYS.CHAT.DELETE_REQUEST,
        data: {
          socketKey,
          userName
        },
        status: {
          code: 200,
          message: 'Chat request deleted succesfuly!'
        }
      }),
      NOT_FOUND: JSON.stringify({
        key: SE_KEYS.CHAT.DELETE_REQUEST,
        data: {},
        status: {
          code: 404,
          message: 'Chat request with that user is not found or user does not exist!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.CHAT.DELETE_REQUEST,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    },
    CLOSE: {
      SUCCESS: JSON.stringify({
        key: SE_KEYS.CHAT.CLOSE,
        data: {},
        status: {
          code: 200,
          message: 'Chat successfuly closed!'
        }
      }),
      NOT_IN_CHAT: JSON.stringify({
        key: SE_KEYS.CHAT.CLOSE,
        data: {},
        status: {
          code: 401,
          message: 'You are currently not in chat with anyone!'
        }
      }),
      ERROR: JSON.stringify({
        key: SE_KEYS.CHAT.CLOSE,
        data: {},
        status: {
          code: 500,
          message: 'Server error! Something went wrong!'
        }
      })
    }
  },
  IE: {
    MESSAGE_RECEIVED: (socketKey, message) => JSON.stringify({
      key: IE_KEYS.CHAT.MESSAGE_RECEIVED,
      data: {
        socketKey,
        message
      }
    }),
    CHAT_REQUEST: (socketKey, userName) => JSON.stringify({
      key: IE_KEYS.CHAT.CHAT_REQUEST,
      data: {
        socketKey,
        userName
      }
    }),
    CLOSED: (socketKey, userName) => JSON.stringify({
      key: IE_KEYS.CHAT.CLOSED,
      data: {
        socketKey,
        userName
      }
    })
  }
};

module.exports = {
  GENERAL_MESSAGES,
  USER_MESSAGES,
  CHAT_MESSAGES
};
