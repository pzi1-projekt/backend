FROM node:14-alpine

EXPOSE 5000

WORKDIR /socket-chat

COPY package.json package-lock.json ./

RUN npm install --production

COPY . .

CMD node index.js