const validateCommand = (key) => {
  if (key && typeof key === 'string') {
    const parts = key.split('/');
    return parts[0] === 'ce' ? parts : false;
  }
  return false;
};

module.exports = {
  validateCommand
};
