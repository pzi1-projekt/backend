const User = require('../classes/user');
const { USER_MESSAGES } = require('../websocket/messages');

const UsersClosure = (() => {
  const users = [];

  function createUser(socketKey, ws) {
    // Create user in closure
    const user = new User(socketKey, ws);
    users.push(user);

    // Inform subscribed users about deleted one
    users.forEach((userFromList) => {
      if (userFromList.subscribedToUsersList && userFromList.socketKey !== user.socketKey) {
        const usersList = getAllUsers(userFromList.socketKey).map(userFromList2 => ({ name: userFromList2.name, socketKey: userFromList2.socketKey }));
        userFromList.ws.send(USER_MESSAGES.SE.SUBSCRIBE_LIST.SUCCESS(usersList));
      }
    });
    return user;
  }

  function deleteUserByKey(socketKey) {
    let indexOfUser = -1;
    users.find((user, index) => {
      if (user.socketKey === socketKey) {
        indexOfUser = index;
        return true;
      }
      indexOfUser = index;
      return false;
    });

    if (indexOfUser !== -1) {
      users.splice(indexOfUser, 1);

      // Inform subscribed users about deleted one
      users.forEach((user) => {
        if (user.subscribedToUsersList) {
          const usersList = getAllUsers(user.socketKey).map(userFromList => ({ name: userFromList.name, socketKey: userFromList.socketKey }));
          user.ws.send(USER_MESSAGES.SE.SUBSCRIBE_LIST.SUCCESS(usersList));
        }
      });
      return true;
    }
    return false;
  }

  function getUserByKey(socketKey) {
    return users.find((user) => {
      if (socketKey === user.socketKey) {
        return user;
      }
      return false;
    });
  }

  function informUsers(message, toExcludeUsers) {
    if (toExcludeUsers) {
      if (typeof toExcludeUsers === 'object') {
        return users.forEach((user) => {
          if (!toExcludeUsers.includes(user.socketKey)) {
            user.ws.send(message);
          }
        });
      }

      return users.forEach((user) => {
        if (toExcludeUsers !== user.socketKey) {
          user.ws.send(message);
        }
      });
    }
    return users.forEach(user => user.ws.send(message));
  }

  function getAllUsers(excludeUsers) {
    if (excludeUsers) {
      if (typeof excludeUsers === 'object') {
        return users.filter(user => !excludeUsers.includes(user.socketKey));
      }

      return users.filter(user => excludeUsers !== user.socketKey);
    }
    return users;
  }

  return {
    createUser,
    deleteUserByKey,
    getUserByKey,
    informUsers,
    getAllUsers
  };
})();

module.exports = UsersClosure;
