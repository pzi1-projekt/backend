const Chat = require('../classes/chat');
const ChatRequest = require('../classes/chatRequest');

const ChatClosure = (() => {
  const chats = [];
  const chatRequests = [];

  function createChat(user1, user2) {
    try {
      chats.push(new Chat(user1, user2));
      return true;
    } catch (e) {
      return false;
    }
  }

  function deleteChat(user) {
    let indexOfChat = -1;
    chats.find((chat, index) => {
      if (chat.user1.socketKey === user.socketKey || chat.user2.socketKey === user.socketKey) {
        indexOfChat = index;
        return true;
      }
      return false;
    });

    if (indexOfChat !== -1) {
      chats.splice(indexOfChat, 1);
      return true;
    }
    return false;
  }

  function getChatRecipient(userSending) {
    let toUser = false;
    chats.find((chat) => {
      if (chat.user1.socketKey === userSending.socketKey) {
        toUser = chat.user2;
        return true;
      } else if (chat.user2.socketKey === userSending.socketKey) {
        toUser = chat.user1;
        return true;
      }
      return false;
    });
    return toUser;
  }

  function createChatRequest(fromUser, toUser) {
    if (chatRequests.length && chatRequests.find(req => req.from.socketKey === fromUser.socketKey && req.to.socketKey === toUser.socketKey || req.from.socketKey === toUser.socketKey && req.to.socketKey === fromUser.socketKey)) {
      return false;
    }
    const chatRequest = new ChatRequest(fromUser, toUser);
    chatRequests.push(chatRequest);

    // Delete the request after 20 seconds if the user did not respond to this request
    setTimeout(() => {
      chatRequests.find((req, index) => {
        if (req.from.socketKey === fromUser.socketKey && req.to.socketKey === toUser.socketKey) {
          chatRequests.splice(index, 1);
          return true;
        }
        return false;
      });
    }, 20000);

    return true;
  }

  function declineChatRequest(socketKey) {
    let indexOfChatRequest = -1;
    chatRequests.find((chatRequest, index) => {
      if (chatRequest.from.socketKey === socketKey) {
        indexOfChatRequest = index;
        return true;
      }
      return false;
    });

    if (indexOfChatRequest !== -1) {
      chatRequests.splice(indexOfChatRequest, 1);
      return true;
    }
    return false;
  }

  function acceptChatRequest(socketKey) {
    let indexOfChatRequest = -1;
    chatRequests.find((chatRequest, index) => {
      if (chatRequest.from.socketKey === socketKey) {
        indexOfChatRequest = index;
        return true;
      }
      return false;
    });

    if (indexOfChatRequest !== -1) {
      chatRequests.splice(indexOfChatRequest, 1);
      return true;
    }
    return false;
  }

  function getUserChat(user) {
    let isUser1 = false;
    const foundChat = chats.find((chat) => {
      if (chat.user1.socketKey === user.socketKey) {
        isUser1 = true;
        return true;
      } else if (chat.user2.socketKey === user.socketKey) {
        isUser1 = false;
        return true;
      }
      return false;
    });

    if (foundChat) {
      return {
        foundChat,
        isUser1
      };
    }
    return { foundChat: false, isUser1: false };
  }

  function deleteChatRequest(user) {
    let indexOfChatRequest = -1;
    chatRequests.find((chatRequest, index) => {
      if (chatRequest.from.socketKey === user.socketKey || chatRequest.to.socketKey === user.socketKey) {
        indexOfChatRequest = index;
        return true;
      }
      return false;
    });

    if (indexOfChatRequest !== -1) {
      chatRequests.splice(indexOfChatRequest, 1);
      return true;
    }
    return false;
  }

  return {
    createChat,
    deleteChat,
    getChatRecipient,
    createChatRequest,
    acceptChatRequest,
    declineChatRequest,
    deleteChatRequest,
    getUserChat
  };
})();

module.exports = ChatClosure;
