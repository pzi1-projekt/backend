const { CHAT_MESSAGES, GENERAL_MESSAGES, USER_MESSAGES } = require('./websocket/messages');
const ChatClosure = require('./closures/chat');
const UsersClosure = require('./closures/users');
const { validateCommand } = require('./helpers');
const WebSocket = require('ws');
const websocketEventHandler = require('./websocket');

const wss = new WebSocket.Server({
  port: 5000
});

console.log('Websocket server listening on 5000');

wss.on('connection', (ws, req) => {
  const connectionSocketKey = req.headers['sec-websocket-key'];
  console.log('New user: ', connectionSocketKey);

  // Create user
  const user = UsersClosure.createUser(connectionSocketKey, ws);

  // Inform user about his credidentials
  ws.send(USER_MESSAGES.IE.INFORMATION(user.socketKey, user.name));

  ws.on('message', (data) => {
    // Try to parse payload, if JSON
    try {
      if (data.toString() === 'keep alive') return;
      const { key: eventKey, data: eventData } = JSON.parse(data);
      const keyParts = validateCommand(eventKey);
      if (keyParts) {
        // Continue to command switch
        websocketEventHandler(
          user, eventKey, keyParts[1], eventData
        );
      } else throw new Error('Wrong message format');
    } catch (e) {
      // Wrong message format, inform the client
      user.ws.send(GENERAL_MESSAGES.WRONG_MESSAGE_FORMAT);
    }
  });

  ws.on('close', () => {
    const { name } = UsersClosure.getUserByKey(connectionSocketKey);
    console.log(`[${name}][${connectionSocketKey}] disconnected '`);
    // Delete chatRequest if exists
    ChatClosure.deleteChatRequest(user);
    // Delete chat if exists
    const { foundChat, isUser1 } = ChatClosure.getUserChat(user);
    if (foundChat) {
      if (isUser1) {
        foundChat.user2.ws.send(CHAT_MESSAGES.IE.CLOSED(foundChat.user1.socketKey, foundChat.user1.name));
      } else {
        foundChat.user1.ws.send(CHAT_MESSAGES.IE.CLOSED(foundChat.user2.socketKey, foundChat.user2.name));
      }
      ChatClosure.deleteChat(user);
    }
    // Inform subscribed users about disconnect
    UsersClosure.deleteUserByKey(connectionSocketKey);
  });
});
